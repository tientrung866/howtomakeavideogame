﻿using System;
using UnityEngine;
using UnityEngine.UI;
public class Score : MonoBehaviour
{
    //[SerializeField] GameObject player;
    [SerializeField] Text scoreText;
    private static bool isGameOver;

    private void Start()
    {
        isGameOver = false;
    }

    private void OnCollisionEnter(Collision collisionInfo)
    {
        if (collisionInfo.collider.CompareTag("Obstacle")||(transform.position.y < 0f))
        {
            Score.isGameOver = true;
            Debug.Log("Gameover");
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (!isGameOver)
        {
            scoreText.text = Time.time.ToString("0");
        }
    }
}
